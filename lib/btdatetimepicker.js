(function($){
  Drupal.behaviors.btdatetimepicker = {
    attach: function (context, settings) {
      $('.btcalendar').datetimepicker({
        locale: moment.locale('IT'),
        format: 'DD/MM/YYYY'
      });
    }
  };
  
})(jQuery);  

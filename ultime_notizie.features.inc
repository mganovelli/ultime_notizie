<?php
/**
 * @file
 * ultime_notizie.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ultime_notizie_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ultime_notizie_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ultime_notizie_node_info() {
  $items = array(
    'notizia' => array(
      'name' => t('Notizia'),
      'base' => 'node_content',
      'description' => t('Una notizia semplice'),
      'has_title' => '1',
      'title_label' => t('Titolo notizia'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
